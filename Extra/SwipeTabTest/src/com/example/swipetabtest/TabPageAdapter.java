package com.example.swipetabtest;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class TabPageAdapter extends FragmentStatePagerAdapter{
	
	public TabPageAdapter(FragmentManager fm){
		super(fm);		
	}
	
	@Override
	public Fragment  getItem(int i){
		
		 switch (i) {
	        case 0:
	            //Fragement for Android Tab
	            return new Android();
	        case 1:
	           //Fragment for Ios Tab
	            return new IOS();
	        case 2:
	            //Fragment for Windows Tab
	            return new Windows();
	        }
	    return null;
	}
	
	@Override
	  public int getCount() {
	    // TODO Auto-generated method stub
	    return 3; //No of Tabs
	  }

}
